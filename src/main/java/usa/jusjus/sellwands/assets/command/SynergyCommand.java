package usa.jusjus.sellwands.assets.command;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import usa.jusjus.sellwands.Core;

public abstract class SynergyCommand {

    @Getter
    private final Core plugin;
    @Getter
    private final boolean consoleAllowed;
    @Getter
    private final String description;
    @Getter
    private final String[] aliases;
    private String[] playerUsage, consoleUsage;
    @Getter
    @Setter
    private double cooldown = 1.0;

    public SynergyCommand(Core plugin, String description, boolean consoleAllowed, String... aliases) {
        this.plugin = plugin;
        this.consoleAllowed = consoleAllowed;
        this.description = description;
        this.aliases = aliases;
        this.playerUsage = aliases;
        this.consoleUsage = aliases;
    }

    public void setPlayerUsage(String... usage) {
        this.playerUsage = usage;
    }

    public void setConsoleUsage(String... usage) {
        this.consoleUsage = usage;
    }

    public abstract void execute(Player player, String command, String[] args);
    public abstract void execute(ConsoleCommandSender sender, String command, String[] args);

    public void sendUsageMessage(Player player) {
        String usageString = "";

        if(playerUsage.length != 0) {
            for(String s : playerUsage)
                usageString+=s + " ";
        }

        player.sendMessage("§cUsage: /" + aliases[0] + (playerUsage.length != 0 ? " " + usageString.trim() : ""));
    }

    public void sendUsageMessage(ConsoleCommandSender console) {
        String usageString = "";

        if(consoleUsage.length != 0) {
            for(String s : consoleUsage)
                usageString+=s + " ";
        }

        console.sendMessage("Usage: /" + aliases[0] + (consoleUsage.length != 0 ? " " + usageString.trim() : ""));
    }
}
