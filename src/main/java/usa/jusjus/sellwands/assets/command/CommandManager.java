package usa.jusjus.sellwands.assets.command;

import com.google.common.collect.Lists;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;
import usa.jusjus.sellwands.Module;
import usa.jusjus.sellwands.Core;
import usa.jusjus.sellwands.assets.command.listener.TabCompleteListener;
import usa.jusjus.sellwands.assets.utilities.UtilString;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CommandManager extends Module implements Listener {

    private final List<SynergyCommand> commands = Lists.newArrayList();
    private Map<String, SynergyCommand> knownCommands;

    public CommandManager(Core plugin) {
        super(plugin, "Command Manager");

        if (!getPlugin().getVersionManager().checkVersion(1.8)) {
            registerListener(
                new TabCompleteListener()
            );
        }

//        Message.add(new HashMap<String, Object>(){{
//            put("player.not_found", "Could not find %player% %color_chat% %attempt%");
//        }});

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommand(PlayerCommandPreprocessEvent e) {
        List<String> args = UtilString.convert(e.getMessage().split(" "));
        String cmd = args.get(0).replaceAll("/", "");

        for(SynergyCommand command : commands) {
            if(Arrays.asList(command.getAliases()).contains(cmd.toLowerCase())) {
                e.setCancelled(true);
                args.remove(0);
                command.execute(e.getPlayer(), cmd, args.toArray(new String[args.size()]));
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommand(ServerCommandEvent e) {
        if (e.getSender() instanceof ConsoleCommandSender){
            List<String> args = UtilString.convert(e.getCommand().split(" "));
            String cmd = args.get(0).replaceAll("/", "");
            for(SynergyCommand command : commands) {
                if (command.isConsoleAllowed()) {
                    if (Arrays.asList(command.getAliases()).contains(cmd.toLowerCase())) {
                        e.setCancelled(true);
                        args.remove(0);
                        command.execute(((ConsoleCommandSender) e.getSender()), cmd, args.toArray(new String[args.size()]));
                        return;
                    }
                }
            }
        }
    }

    public boolean isCommand(String command){
        for(SynergyCommand cmd : commands){
            for(String alias : cmd.getAliases()){
                if (alias.equalsIgnoreCase(command)){
                    return true;
                }
            }
        }
        return false;
    }

    public List<SynergyCommand> getCommands() {
        return commands;
    }
}
