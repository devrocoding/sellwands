package usa.jusjus.sellwands;

import lombok.Getter;
import org.bukkit.event.Listener;
import usa.jusjus.sellwands.assets.command.SynergyCommand;

import java.util.Arrays;

public abstract class Module implements Listener {

    @Getter
    private final Core plugin;
    @Getter
    private final String name;
    @Getter
    private boolean disabled = false;
    @Getter
    public static int total = 0;

    public Module(Core plugin, String name) {
        this.plugin = plugin;
        this.name = name;
        total++;

        plugin.getServer().getPluginManager().registerEvents(this, plugin);

        getPlugin().getModules().add(this);
    }

    public void registerListener(Listener... listeners) {
        Arrays.stream(listeners).forEach(listener -> getPlugin().getServer().getPluginManager().registerEvents(listener, plugin));
    }

    public void registerCommand(SynergyCommand... commands) {
        for (SynergyCommand command : commands) {
            plugin.getCommandManager().getCommands().add(command);
        }
    }

    public String getShortname(){
        return getName().split(" ")[0];
    }

    public void disable(){
        this.disabled = true;
    }
}
