package usa.jusjus.sellwands.sellwand.command;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import usa.jusjus.sellwands.Core;
import usa.jusjus.sellwands.assets.command.SynergyCommand;

public class CommandSellWand extends SynergyCommand {

    public CommandSellWand(Core plugin){
        super(plugin, "", false, "sellwand");
    }

    @Override
    public void execute(Player player, String command, String[] args) {
        if (player.hasPermission("sellwand.give")) {
            if (args.length > 0 && args.length < 4){
                if (args[0].equalsIgnoreCase("give")){
                    try{
                        int amount = Integer.valueOf(args[1]);
                        Player target = Bukkit.getPlayer(args[2]);
                        if (target == null){
                            target = player;
                        }
                        if (amount > target.getInventory().getSize()){
                            player.sendMessage("§cYou can't give more then you can own!");
                            return;
                        }
                        player.getInventory().addItem(Core.getPlugin().getSellWandManager().getSallwand(amount));
                    }catch (NumberFormatException ex){
                        player.sendMessage("§c"+args[1]+" is not a number!");
                    }
                }
            }
            sendUsageMessage(player);
        }
    }

    @Override
    public void execute(ConsoleCommandSender sender, String command, String[] args) {

    }
}
