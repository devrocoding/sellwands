package usa.jusjus.sellwands.sellwand;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import usa.jusjus.sellwands.Core;
import usa.jusjus.sellwands.Module;
import usa.jusjus.sellwands.assets.utilities.ItemBuilder;
import usa.jusjus.sellwands.sellwand.listener.PlayerInteractListener;

public class SellWandManager extends Module {

    public SellWandManager(Core plugin){
        super(plugin, "SellWand Manager");

        registerListener(
            new PlayerInteractListener()
        );
    }

    public ItemStack getSallwand(int amount){
        return new ItemBuilder(Material.BLAZE_ROD)
                .setName("§6§lSellWand - Sell your items")
                .build();

    }

    public ItemStack getSellwand(){
        return getSallwand(1);
    }
}
