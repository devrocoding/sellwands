package usa.jusjus.sellwands.sellwand.listener;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import usa.jusjus.sellwands.Core;
import usa.jusjus.sellwands.economy.EconomyManager;
import usa.jusjus.sellwands.sellwand.SellWandManager;

public class PlayerInteractListener implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e){
        if (e.getAction() == Action.RIGHT_CLICK_AIR){
            SellWandManager sm = Core.getPlugin().getSellWandManager();
            EconomyManager em = Core.getPlugin().getEconomyManager();
            if (e.getItem() == null
                    || e.getClickedBlock() == null
                    || e.getItem() != sm.getSellwand()) return;

            Block b = e.getClickedBlock();
            if (b instanceof Chest){
                Chest c = (Chest)b;

                double total = em.sellAll(e.getPlayer(), c.getBlockInventory());
                if (total <= 0){
                    e.getPlayer().closeInventory();
                    e.getPlayer().sendMessage(
                        ChatColor.translateAlternateColorCodes(
                                '&',
                            Core.getPlugin().getMessages().get().getString("messages.sell_success")
                                    .replaceAll("\\{TOTAL}", Double.toString(total)
                                    )
                        )
                    );
                }else{
                    e.getPlayer().sendMessage("§cCannot sell the items in this chest");
                }
            }
        }
    }

}
