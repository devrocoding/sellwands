package usa.jusjus.sellwands;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.plugin.java.JavaPlugin;
import usa.jusjus.sellwands.assets.YMLFile;
import usa.jusjus.sellwands.assets.command.CommandManager;
import usa.jusjus.sellwands.economy.EconomyManager;
import usa.jusjus.sellwands.sellwand.SellWandManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Core extends JavaPlugin {

    @Getter
    public List<Module> modules = new ArrayList<>();
    @Getter @Setter
    private static Core plugin;

    @Getter
    private CommandManager commandManager;
    @Getter
    private SellWandManager sellWandManager;
    @Getter
    private EconomyManager economyManager;

    @Getter
    public YMLFile messages;

    @Override
    public void onEnable(){
        setPlugin(this);

        if (!getServer().getPluginManager().isPluginEnabled("Essentials")
                || !getServer().getPluginManager().isPluginEnabled("Vault")){
            System.out.println("Dependencies not found. Disabling");
            getPluginLoader().disablePlugin(this);
            return;
        }

        this.messages = new YMLFile(
                getDataFolder().toString(),
                "messages"
        );

        this.messages.set(new HashMap<String, Object>(){{
            put("messages.sell_success", "&aYou have received ${TOTAL} bucks");
        }});

        this.commandManager = new CommandManager(this);
        this.economyManager = new EconomyManager(this);
        this.sellWandManager = new SellWandManager(this);
    }

}
