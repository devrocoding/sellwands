package usa.jusjus.sellwands.economy;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import usa.jusjus.sellwands.Core;
import usa.jusjus.sellwands.Module;
import usa.jusjus.sellwands.assets.YMLFile;
import usa.jusjus.sellwands.assets.utilities.ItemBuilder;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class EconomyManager extends Module {

    private Map<ItemStack, Double> worth = new HashMap<>();
    private final String dataFolder = "plugins"+File.separator+"essentials";
    private Economy economy;

    public EconomyManager(Core plugin){
        super(plugin, "Economy Manager");

        try{
            RegisteredServiceProvider<Economy> rsp = getPlugin().getServer().getServicesManager().getRegistration(Economy.class);
            this.economy = rsp.getProvider();
        }catch (NullPointerException ex){
            ex.printStackTrace();
            //TODO: Disable the plugin, since the economy is not accessible
        }

        initWorth();
    }

    private void initWorth(){
        try{
            YMLFile file = new YMLFile(this.dataFolder, "worth");
            FileConfiguration c = file.get();
            final String path = "worth";

            for(String material_str : c.getConfigurationSection("worth").getKeys(false)){
                Map<String, Object> map = c.getConfigurationSection(path+"."+material_str).getValues(true);
                final Material material = Material.matchMaterial(material_str);
                if (map.keySet().size() > 0){
                    // Has data values
                    for(String data_str : c.getConfigurationSection("worth").getKeys(false)){
                        int data = Integer.getInteger(data_str);
                        double price = c.getDouble(path+"."+material_str+"."+data_str);
                        worth.put(
                                new ItemBuilder(material).setDamage(data).build(),
                                price
                        );
                    }
                    // If it has not found any data types, then add the normal ones.
                    return;
                }
                double price = c.getDouble(path+"."+material_str);
                worth.put(
                        new ItemBuilder(material).build(),
                        price
                );
            }
        }catch (NullPointerException | NumberFormatException ex){
            ex.printStackTrace();
        }
    }

    public double sellAll(Player p, Inventory inventory){
        double total = 0D;
        for(ItemStack item : inventory.getContents()){
            if (worth.containsKey(item)){
                final double price = worth.get(item);
                total += price;
                inventory.remove(item);
            }
        }
        this.economy.depositPlayer(p, total);
        return total;
    }

}
